package org.exadel.exadelkarateautotests;

import com.intuit.karate.junit5.Karate;

public class TestRunner {

    @Karate.Test
    Karate testPing() {
        return Karate.run("features/ping")
            .relativeTo(getClass());
    }

}
