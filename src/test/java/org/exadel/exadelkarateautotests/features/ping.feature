Feature: Ping a httpbin

  Background:
    * def baseURL = 'https://httpbin.org/'

  Scenario: check status code 200
    Given url baseURL
    When method GET
    Then status 200