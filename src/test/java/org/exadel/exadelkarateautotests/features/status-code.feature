Feature: Autotests for check status code

  Background:
    * def baseURL = 'https://httpbin.org/'

  Scenario Outline: check status code <statusCode> with GET method
    Given url baseURL + 'status/' + <statusCode>
    When method GET
    Then status <statusCode>
    Examples:
    | statusCode |
    | 200        |
    | 404        |
    | 500        |