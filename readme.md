### Technology

* [Java](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
* [Maven](https://maven.apache.org/)
* [Karate](https://github.com/intuit/karate)

### Run parameters

* Run smoke tests
```shell
mvn test
```
* Run feature
```shell
mvn test "-Dkarate.options=features/status-code.feature"
```